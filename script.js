const items = [
    {
        name: 'Mes documents',
        type: 'folder',
        size: 540,
        date: new Date(2019, 11, 17, 03, 24, 00),
        author: 'Antoine Parent'
    },
    {
        name: 'Monfichier.txt',
        type: 'file',
        size: 21,
        date: new Date(2018, 3, 14, 17, 31, 40),
        author: 'Mélanie Vidal'
    },
    {
        name: 'Mes images',
        type: 'folder',
        size: 7500,
        date: new Date(2019, 7, 16, 14, 7, 5),
        author: 'Florimond Parent'
    },
    {
        name: 'Mesadresses.xls',
        type: 'file',
        size: 543,
        date: new Date(2015, 8, 11, 10, 7, 51),
        author: 'Charles Parent'
    },
    {
        name: 'image1.jpg',
        type: 'file',
        size: 15000,
        date: new Date(2011, 5, 21, 17, 12, 20),
        author: 'Hélène Cardona'
    },
    {
        name: 'installation-manual.pdf',
        type: 'file',
        size: 8400,
        date: new Date(2016, 6, 7, 10, 41, 10),
        author: 'Jeanne Samson'
    },
    {
        name: 'image4.jpg',
        type: 'file',
        size: 21000,
        date: new Date(2013, 0, 12, 13, 47, 3),
        author: 'Marie Fontanieu'
    },
    {
        name: 'construire_une_ruche.pdf',
        type: 'file',
        size: 8400,
        date: new Date(2018, 10, 11, 7, 35, 8),
        author: 'Antoine Parent'
    },
    {
        name: 'Vidéos',
        type: 'folder',
        size: 150000,
        date: new Date(2019, 9, 30, 21, 55, 13),
        author: 'Melissa Boulanger'
    }
]

//Conversion
const item2Array = (obj) => {
    let temp_array = Object.keys(obj).map(key => {
        return obj[key]
    })
    return temp_array
}

//let data = Object.values(items).map(obj => item2Array(obj))
let data = items

//CONTAINER
//LINE
function drawLine(data) {
    let container = document.getElementById('container')
    container.innerHTML = ''
    container.classList.add('container')
    container.style.flexDirection = 'column'
    document.body.appendChild(container)

    for (let i = 0; i < data.length; i++) {

        let bloc = document.createElement('div')
        bloc.classList.add('lineBox')

        //Image
        let img = document.createElement('img')
        if (data[i].type === 'folder') {
            img.setAttribute('src', 'img/folder.svg');
            img.setAttribute('alt', 'folder');
        } else if (data[i].type === 'file') {
            img.setAttribute('alt', 'file');
            img.setAttribute('src', 'img/file.svg');
        }
        bloc.appendChild(img)

        //Name
        let name = document.createElement('div')
        name.style.width = '40%'
        name.innerText = data[i].name
        bloc.appendChild(name)

        //Author
        let author = document.createElement('div')
        author.style.width = '20%'
        author.innerText = data[i].author
        bloc.appendChild(author)

        //Date
        let date = document.createElement('div')
        date.style.width = '20%'
        date.innerText = data[i].date.toDateString()
        bloc.appendChild(date)

        //Size
        let size = document.createElement('div')
        size.style.width = '10%'
        size.style.textAlign = 'right'

        let size_value = data[i].size
        if (size_value >= 1000) {
            size_value = size_value / 1000 + 'Mo'
        } else {
            size_value += 'Ko'
        }
        size.innerText = size_value
        bloc.appendChild(size)

        //adding bloc
        container.appendChild(bloc)
    }
}

//GRID
function drawGrid(data) {
    let container = document.getElementById('container')
    container.innerHTML = ''
    container.classList.add('container')
    container.style.flexDirection = 'row'
    document.body.appendChild(container)

    for (let i = 0; i < data.length; i++) {

        let bloc = document.createElement('div')
        bloc.classList.add('gridBox')

        //TYPE
        let type = document.createElement('img')
        if (data[i].type === 'folder') {
            type.setAttribute('src', 'img/folder.svg');
            type.setAttribute('alt', 'folder');
        } else if (data[i].type === 'file') {
            type.setAttribute('alt', 'file');
            type.setAttribute('src', 'img/file.svg');
        }
        bloc.appendChild(type)

        //Name
        let name = document.createElement('div')
        name.innerText = data[i].name
        bloc.appendChild(name)
    
        container.appendChild(bloc)

        /*var title = data_array[i][4]
        type.setAttribute('title', title);
        type.setAttribute('height', '150px');
        type.setAttribute('width', '150px');
        document.body.appendChild(type);    

        //NOM
        let name = document.createElement('h2')
        name.innerText = data_array[i][0]
        document.body.appendChild(name)

        //POIDS
        let size = document.createElement('p')
        size.innerText = data_array[i][2]
        document.body.appendChild(size)

        console.log(data_array[i]) */
    }
}
//Sort by number property (in object)
const sortByNumProperty = (data, property) => {
    return data.sort((elt1, elt2) => elt2[property] - elt1[property])
}

//Sort by string property (in object)
//https://ourcodeworld.com/articles/read/764/how-to-sort-alphabetically-an-array-of-objects-by-key-in-javascript
function sortByStrProperty(property) {
    var sortOrder = 1;

    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a, b) {
        if (sortOrder == -1) {
            return b[property].localeCompare(a[property]);
        } else {
            return a[property].localeCompare(b[property]);
        }
    }
}
//Button Grid
let grid_button = document.createElement('button')
grid_button.innerText = 'Grille'
grid_button.addEventListener('click', () => { drawGrid(data) })
document.body.appendChild(grid_button)

//Button Line
let line_button = document.createElement('button')
line_button.innerText = 'Ligne'
line_button.addEventListener('click', () => { drawLine(data) })
document.body.appendChild(line_button)

//Button Type
let sort_by_type = document.createElement('button')
sort_by_type.innerText = 'Type'
sort_by_type.addEventListener('click', () => { drawLine(data.sort(sortByStrProperty('type'))) })
document.body.appendChild(sort_by_type)

//Button Name
let sort_by_name = document.createElement('button')
sort_by_name.innerText = 'Name'
sort_by_name.addEventListener('click', () => { drawLine(data.sort(sortByStrProperty('name'))) })
document.body.appendChild(sort_by_name)

//Button Author
let sort_by_author = document.createElement('button')
sort_by_author.innerText = 'Author'
sort_by_author.addEventListener('click', () => { drawLine(data.sort(sortByStrProperty('author'))) })
document.body.appendChild(sort_by_author)

//Button Size
let sort_by_size = document.createElement('button')
sort_by_size.innerText = 'Size'
sort_by_size.addEventListener('click', () => { drawLine(sortByNumProperty(data, 'size')) })
document.body.appendChild(sort_by_size)

drawLine(data)